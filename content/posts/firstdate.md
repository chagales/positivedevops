---
title: First Date
date: "2019-03-01"
url: "/firstdate"
description: "This is the resume to my first day in positive devops Bootcamp"
credit: "https://unsplash.com/photos/h-M3O25tyvI"
image: "img/unsplash-photos-h-M3O25tyvI.jpg"
thumbnail: img/unsplash-photos-h-M3O25tyvI.tn-500x500.jpg
classes:
- feature-hyphenate
- feature-justify
- feature-highlight
- feature-tweetquote
categories:
- Demo
---
Breve resumen del primer dia de Bootcamp, donde entre otras cosas tuvimos la suerte de contar con tres personas del sector muy potentes.

- Javier Lafora
- Luis Peralta
- Juan Lupión

<!--more-->

La sesion empeza fuerte, nos sentamos en una salita (con poco aire o mucho calor todo hay que decirlo :) ) y empezamos con una dinámica de presentaciones fuera de lo común, pero que nos indica el grado de implicación en las charlas, es decir bastante elevado, molaaaa!!!!.

A continuación Jacobo da un repaso rapido de la plataforma, y se comentan varios temas del devOps, pero lo interesante esta por llegar.

Aparece Luis y como uno más toma asiento, a partir de aqui empezamos a discutir, desde las implicaciones de un monorepo vs multirepos hasta el porque de los unicornios  (eso si a nivel de lenguajes de programación).

Despues de un merecido descanso pasamos a la mesa redonda (aparece Juan en la escena) y sorpresa primer punto sobre la i, DevOps no es un rol jajajaja.

### Que es DevOps?

Efectivamente DevOps como rol no existe, es una cultura/marco/metodologia de trabajo, donde todo el mundo se preocupa de todo, es decir, no solo por el desarrollo o por poner en producción (y que las cosas no fallen, como bien indica Juan, cuando tienes que hacer guardia te acuerdas muy mucho de desarrollar para facilitar esa guardia) si no un mix de todo, donde el sysadmin de toda la vida desarrolla y viceversa

----

Pero continuando con la mesa redonda, además hablamos del recruiting, si de eso que tanto se habla ultimamente, y al final la conclusión es la que creo que ronda en la cabeza a todo el mundo, continua formandote y manten la pasión que en algún momento puede venir el tren (o buscalo claro).

Terminamos el dia pensando en que las dinamicas pueden estar muy bien, no solo por el contenido (ojo muy interesante) sino por el chute de motivación que te dan cada viernes.


> [Fede Muñoz Babiano](https://twitter.com/chagales)
>
> Disfruta de la vida viviendo el momento, tocando el pasado y bebiendo el futuro.

Read next: [Story's MailChimp subscribe forms and RSS feeds](/mailchimp-features).
